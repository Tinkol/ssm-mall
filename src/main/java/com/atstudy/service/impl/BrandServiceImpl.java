package com.atstudy.service.impl;

import com.atstudy.mapper.BrandMapper;
import com.atstudy.pojo.entity.Brand;
import com.atstudy.service.BrandService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public List<Brand> selectBrand() {
//        PageHelper.startPage(pageNum,pageSize);
        List<Brand> brands = brandMapper.selectBrand();
        return brands;
    }
}
