package com.atstudy.service;

import com.atstudy.pojo.entity.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> selectBrand( );
}
