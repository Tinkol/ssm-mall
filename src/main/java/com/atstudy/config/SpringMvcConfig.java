package com.atstudy.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@ComponentScan({"com.atstudy.controller"})
@EnableWebMvc
public class SpringMvcConfig implements WebMvcConfigurer {



}
