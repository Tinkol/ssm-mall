package com.atstudy.config;

import org.springframework.context.annotation.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import javax.sql.DataSource;
@Import({MybatisConfig.class,JdbcConfig.class})
@ComponentScan("com.atstudy.service")
@Configuration
@PropertySource("classpath:jdbc.properties")
@EnableAspectJAutoProxy
//开启事务管理
@EnableTransactionManagement
public class SpringConfig {
    @Bean("transactionManager")
    //事物管理器注册到Spring
    public DataSourceTransactionManager getDataSourceTransactionManager(DataSource dataSource){
        DataSourceTransactionManager dtm = new DataSourceTransactionManager();
        dtm.setDataSource(dataSource);
        return  dtm;
    }
}
