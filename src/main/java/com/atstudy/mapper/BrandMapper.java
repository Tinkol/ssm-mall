package com.atstudy.mapper;

import com.atstudy.pojo.entity.Brand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BrandMapper {
    List<Brand> selectBrand();
}
