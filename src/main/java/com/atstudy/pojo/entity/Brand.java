package com.atstudy.pojo.entity;


import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
public class Brand {

   private String brandId;
   private String brandName;
   private String brandIntroduction;
   private String brandLogourl;
   private Integer sortno;
   private Date createtime;
   private Date updatetime;

}
