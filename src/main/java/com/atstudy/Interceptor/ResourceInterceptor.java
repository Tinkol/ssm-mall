package com.atstudy.Interceptor;


import com.atstudy.pojo.entity.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ResourceInterceptor extends HandlerInterceptorAdapter {
    private List<String> ignoreUrl;
    public ResourceInterceptor(List<String> ignoreUrl) {
        this.ignoreUrl = ignoreUrl;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        {
            User user=(User)request.getSession().getAttribute("USER_SESSION");
            String uri=request.getRequestURI();
            if (uri.indexOf("login")>=0)
            {
                return true;
            }
            if (user!=null)
            {
                if ("ADMIN".equals(user.getRole()))
                {
                    return true;
                }
                else if (!"ADMIN".equals(user.getRole()))
                for (String url:ignoreUrl)
                {
                    if (uri.indexOf(url)>=0)
                        return true;
                }
            }
            request.setAttribute("msg","您还没有登录");
            request.getRequestDispatcher("/admin/login.jsp").forward(request,response);
            return false;
        }
    }
}
