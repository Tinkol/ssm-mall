import com.atstudy.config.SpringConfig;
import com.atstudy.controller.BrandController;
import com.atstudy.pojo.entity.Brand;
import com.atstudy.service.BrandService;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfig.class)
@Log4j2
public class BrandTest {
    @Autowired
    private BrandService brandService;


    @Test
    public void selectTest(){
        List<Brand> brands = brandService.selectBrand();
    }

}
